#!/bin/bash

# From "man sed"
# Sed is a stream editor.  A stream editor is used to perform basic text
# transformations on an input stream (a file or input from  a  pipeline).
# While  in  some  ways similar to an editor which permits scripted edits
# (such as ed), sed works by making only one pass over the input(s),  and
# is consequently more efficient.  But it is sed's ability to filter text
# in a pipeline which particularly distinguishes it from other  types  of
# editors.

# or add -i to edit inplace
# substitute "god" or "God" with GOD in bible.txt, and print to stdout
#
# without "g" this will replace only first instance in each line
#
# then grep is used to show lines, where "god" (case insensitive) is encountered
sed "s/[gG]od/GOD/g" bible.txt | grep -wi god
