#!/bin/awk -f

# format like following:
# Genesis 1:1	In the beginning God created the heavens and the earth.
# into
# <GENESIS> [1:1] - In the beginning God created the heavens and the earth.

BEGIN {
    FS = "[ :\t]"
};

NR > 3 {
    chapter = $1
    major = $2
    minor = $3

    printf  "<%s> [%s:%s] - ", chapter, major, minor;
    $1 = $2 = $3 = "";
    # trim first spaces, note no "" around regex
    sub(/   /, "", $0);
    printf "%s\n", $0;
};