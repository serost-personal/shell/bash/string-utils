#!/bin/bash

# grep is used for searching words through files.

# from "man grep"
# grep  searches  for  PATTERNS  in  each  FILE.  PATTERNS is one or more
# patterns separated by newline characters, and  grep  prints  each  line
# that  matches a pattern.  Typically PATTERNS should be quoted when grep
# is used in a shell command.

# will search for word "God" (case sensative) through bible.txt,
#   and print those lines
g1=$(grep -w 'God' bible.txt)
g2=$(cat bible.txt | grep -w 'God') # same thing
g1_is_g2=$(test "$g1" = "$g2")      # True ($? returns 0)
