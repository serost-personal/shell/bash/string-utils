#!/bin/awk -f

# Statement blocks run for each line in order (excluding special like BEGIN/END)

# "BEGIN" block is reserved, and always runs first
BEGIN {
    # separate input by WHAT (regex)
    FS = "[ :\t.]"; # space or ":"
    # separate output by WHAT
    OFS = " - "; # by " - " string
};
# this block runs every line, where NR (number line) > 3
# lines count from 1 to N (1, 2, 3, ... N)

# will run for lines that:
#   have a number > 3
#   have "earth" in them
NR > 3 || /earth/ { 
    # values between commas are separated by value in "OFS"
    # NF returns id of last token, or last token + 1 if separator met at the end

    # note: NF returns id of last word
    #       $NF returns last word itself
    print $1, $2 ":" $3 "\t::: " $4 " " $(NF-1) "["NR"]";
};

END {
    # cmd_var returns value itself
    # $cmd_var returns word at the index, or $0 otherwise
    print "Value from command line: " cmd_var
};