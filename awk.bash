#!/bin/bash

# awk is more than just a tool, its a domain specific programming language

# From "man awk"
# he AWK language is useful for manipulation of data files, text retrieval
# and processing,  and  for prototyping and experimenting with algorithms.

# read command from file
head -10 bible.txt | awk -v cmd_var="Hello, world!" -f ./bible-query.awk
